<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">

    {* Title, description, keywords *}
    {block 'seo'}
        {render_meta:raw}
    {/block}

    <link rel="stylesheet" href="{$.assets_public_path('main.css', 'frontend')}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    {* Another head information *}
    {block 'head'}{/block}
</head>
<body>

    {block 'header'}{include '/_parts/header.tpl'}{/block}
    {block 'content'}{/block}
    {block 'core_js'}
        <script src="{$.assets_public_path('vendors.js', 'frontend')}"></script>
        <script src="{$.assets_public_path('main.js', 'frontend')}"></script>
    {/block}
    {render_dependencies_js:raw}
    {render_inline_js:raw}
    {render_dependencies_css:raw}
    {render_inline_css:raw}
    {block 'js'}

    {/block}
{if $.is_debug}<div id="DEBUG"></div>{/if}
</body>
</html>